#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "ilk_defs.h"

int ilk_udp_server_sim(void)
{
    char sim_id[] = "SN:0000000";
    int sockfd;
    char buf[10];
    struct sockaddr_in sendaddr;
    struct sockaddr_in recvaddr;
    int addr_len;
    int broadcast=1;

    if((sockfd = socket(PF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        return -1;
    }

    if((setsockopt(sockfd,SOL_SOCKET,SO_BROADCAST,
        &broadcast,sizeof(broadcast))) == -1)
    {
        perror("setsockopt - SO_SOCKET ");
        return -1;
    }

    printf("socket created\n");

    sendaddr.sin_family = AF_INET;
    sendaddr.sin_port = htons(ILK_UDP_PORT);
    sendaddr.sin_addr.s_addr = INADDR_ANY;
    memset(sendaddr.sin_zero,'\0', sizeof(sendaddr.sin_zero));

    recvaddr.sin_family = AF_INET;
    recvaddr.sin_port = htons(ILK_UDP_PORT);
    recvaddr.sin_addr.s_addr = INADDR_ANY;
    memset(recvaddr.sin_zero,'\0',sizeof(recvaddr.sin_zero));

    if(bind(sockfd, (struct sockaddr*) &recvaddr, sizeof(recvaddr)) == -1)
    {
        perror("bind\n");
        return -1;
    }

    while(1)
    {
        int ret = 0;
        addr_len = sizeof(sendaddr);

        ret = recvfrom(sockfd, buf, sizeof(buf), 0,
            (struct sockaddr *)&sendaddr, (socklen_t *)&addr_len);

        if (ret == -1)
        {
            perror("recv error\n");
        }
        else
        {
            printf("Send addr: %s \n", inet_ntoa(sendaddr.sin_addr));
            printf("Send mesg: %s \n",buf);
        }

        if(sendto(sockfd, sim_id, sizeof(sim_id), 0,
                (struct sockaddr *)&sendaddr, sizeof(sendaddr)) != -1)
        {
            perror("sendto\n");
        }
    }
    return 0;
}


void *sim_server_func(void *arg)
{
    printf("sim server for elb\n");
    ilk_udp_server_sim();
}

int sim_server_main()
{
    static pthread_t sim_server;
    int err;

    //创建线程
    err = pthread_create(&sim_server, NULL, sim_server_func, NULL);
    if(err != 0)
    {
        printf("create thread error: %s\n",strerror(err));
        return 1;
    }

    printf("server thread: pid: %u tid: %u (0x%x)\n",
        (unsigned int)getpid(),
        (unsigned int)pthread_self(),
        (unsigned int)pthread_self());
}

int main(void)
{
    sim_server_main();

    while(1)
    {
        sleep(1000);
    }

    return 0;
}
