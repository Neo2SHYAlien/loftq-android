#ifndef ILK_SRV_H
#define ILK_SRV_H

#include "ilk_defs.h"

typedef struct
{
    char name[128];
    ilk_func_t msg_func;
    int mode;
}ilk_srv_para_t, *pilk_srv_para_t;

int ilk_srv_init(pilk_srv_para_t ppara);
int ilk_srv_ctrl(int onoff);
int ilk_srv_process(void);
int ilk_srv_fini(void);

#endif // ILK_SRV_H
