#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <pthread.h>
#include <string.h>

int main(void)
{
    int ret = 0;
    int length = 0;

    #ifdef ILK_ANDROID
    char ser_name[] = "/dev/socket/mixtile.ilk_srv_port";
    #else
    char ser_name[] = "./mixtile.ilk_srv_port";
    #endif

    struct sockaddr_un server_addr;

    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, ser_name);
    length = strlen(server_addr.sun_path)+sizeof(server_addr.sun_family);

    int client = socket(PF_UNIX, SOCK_DGRAM, 0);
    if(client < 0)
    {
        printf("srv create socket failed !\n");
        return -1;
    }

    printf("srv create socket success !\n");

    ret = connect(client, (struct sockaddr*)&server_addr,
        length);

    if(ret < 0)
    {
        printf("srv connect socket failed !\n");
        return -1;
    }

    printf("srv connect socket success !\n");

    while(1)
    {
        printf("send start scene !\n");
        send(client, "start", sizeof("start"), 0);
        sleep(10);
        printf("send stop scene !\n");
        send(client, "stop", sizeof("start"), 0);
        sleep(10);
    }

    close(client);

    return 0;
}
