#ifndef ILK_ACTION_H
#define ILK_ACTION_H

#include "ilk_defs.h"

int ilk_action_init(void);
int ilk_action_fini(void);
int ilk_action_set(u_int32_t ip, u_int32_t snid);
int ilk_action_process(char* event, int len);

#endif // ILK_ACTION_H
