#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>

#include "ilk_main.h"
#include "ilk_defs.h"
#include "ilk_tcp.h"
#include "ilk_udp.h"
#include "ilk_srv.h"
#include "ilk_gw.h"
#include "ilk_ctrl.h"
#include "ilk_action.h"

#define ILK_MAIN_DEV_MAX 50

typedef struct
{
    int init_flag;
    int update_tcp;
}ilk_main_t, *pilk_main_t;

static ilk_main_t gdc;
static pilk_main_t gpdc = &gdc;

static int ilk_main_udp_handle(void* para, void* data)
{
    pilk_udp_func_para_t ppara = (pilk_udp_func_para_t)para;

    if(NULL != ppara)
    {
        gpdc->update_tcp++;
        ilk_action_set(ppara->ip, ppara->snid);
    }

    return 0;
}


static int ilk_main_srv_handle(void* para, void* data)
{
    char* msg = (char*)para;
    int len = *(int*)data;

    ilk_action_process(msg, len);

    return 0;
}


int ilk_main_daemon(void)
{
    pid_t pid;

    pid = fork();

    if(pid == -1)
    {
        printf("fork error\n");
    }

    if(setsid() == -1)
    {
        printf("setsid error\n");
    }

    chdir("/");

    umask(0);

    return 0;
}


int ilk_main_init(void)
{
    ilk_tcp_para_t tcp_para =
    {
        ILK_TCP_PORT,
    };

    ilk_udp_para_t udp_para =
    {
        ILK_UDP_BDIP,
        ILK_UDP_PORT,
        ilk_main_udp_handle,
        2,
    };

    ilk_srv_para_t srv_para =
    {
        ILK_SRV_SOCKET_NAME,
        ilk_main_srv_handle,
        1,
    };

    int ret = 0;

    if(gpdc->init_flag == 1)
    {
        printf("ilk main has been inited!");
        return -1;
    }

    memset(gpdc, 0, sizeof(ilk_main_t));

    // udp service init
    ret = ilk_udp_init(&udp_para);
    if(ret != 0)
    {
        printf("ilk udp init failed");
        return -1;
    }

    // tcp service init
    ret = ilk_tcp_init(&tcp_para);
    if(ret != 0)
    {
        ilk_udp_fini();
        printf("ilk tcp init failed");
        return -1;
    }

    // action init
    ilk_action_init();

    ret = ilk_srv_init(&srv_para);
    if(ret != 0)
    {
        ilk_udp_fini();
        ilk_tcp_fini();
        printf("ilk srv init failed");
        return -1;
    }

    // local service init
    gpdc->init_flag = 1;

    return 0;
}

int ilk_main_loop(void)
{
    if(1 == gpdc->init_flag)
    {
        ilk_srv_process();
    }
    
    return 0;
}

int ilk_main_fini(void)
{
    ilk_srv_fini();
    ilk_udp_fini();
    ilk_tcp_fini();
    ilk_action_fini();
    return 0;
}


