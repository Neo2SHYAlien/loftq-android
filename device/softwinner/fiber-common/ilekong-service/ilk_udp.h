#ifndef ILK_UDP_H
#define ILK_UDP_H
#include "ilk_defs.h"

typedef struct
{
    char ip[40];
    u_int16_t port;
    ilk_func_t update_func;
    u_int32_t update_freq; //check frequency, per second
}ilk_udp_para_t, *pilk_udp_para_t;

typedef struct
{
    u_int32_t ip;
    u_int32_t snid;
}ilk_udp_func_para_t, *pilk_udp_func_para_t;

int ilk_udp_init(pilk_udp_para_t ppara);
int ilk_udp_query(u_int32_t* ip, u_int32_t* snid);
int ilk_udp_ctrl();
int ilk_udp_fini();

#endif // ILK_UDP_H
